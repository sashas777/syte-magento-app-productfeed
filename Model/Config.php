<?php

/**
 * Syte_ProductFeed
 */

declare(strict_types=1);

namespace Syte\ProductFeed\Model;

use Syte\Core\Model\Config as CoreConfig;
use Syte\Core\Model\Constants;

class Config extends CoreConfig
{
    /** @const string */
    private const SYTE_CONFIG_AREA = Constants::SYTE_CONFIG_PATH_PRODUCT_FEED;

    /**
     * Get config area value by short fieldname
     *
     * @param string $fieldShort
     * @param null|int $storeId
     *
     * @return null|int|string
     */
    private function getAreaConfig(string $fieldShort, $storeId = null)
    {
        return $this->getConfigValue(self::SYTE_CONFIG_AREA . $fieldShort, $storeId);
    }

    /**
     * Get active status
     *
     * @param int $storeId
     *
     * @return bool
     */
    public function isServiceActive(int $storeId): bool
    {
        $isActive = (int)$this->getAreaConfig('active', $storeId);

        return $isActive ? true : false;
    }

    /**
     * Get log status
     *
     * @param int $storeId
     *
     * @return bool
     */
    public function isLogActive(int $storeId): bool
    {
        $isActive = (int)$this->getAreaConfig('log', $storeId);

        return $isActive ? true : false;
    }

    /**
     * Get config connection type
     *
     * @param int $storeId
     *
     * @return bool
     */
    public function getConnectionType(int $storeId): int
    {
        return (int)$this->getAreaConfig('connection', $storeId);
    }

    /**
     * Get ftp settings
     *
     * @param int $storeId
     *
     * @return array
     */
    public function getFtpConfig(int $storeId): array
    {
        return [
            'is_ftp' => ($this->getConnectionType($storeId) == Constants::SYTE_CONFIG_CONNECTION_SFTP),
            'host' => (string)$this->getAreaConfig('ftp_host', $storeId),
            'port' => (int)$this->getAreaConfig('ftp_port', $storeId),
            'user' => (string)$this->getAreaConfig('ftp_user', $storeId),
            'password' => (string)$this->getAreaConfig('ftp_password', $storeId),
            'path' => (string)$this->getAreaConfig('ftp_path', $storeId),
        ];
    }

    /**
     * Get schedule settings
     *
     * @return array
     */
    public function getScheduleConfig(): array
    {
        return [
            'schedule_type' => (int)$this->getAreaConfig('schedule_type'),
            'schedule_date' => (string)$this->getAreaConfig('schedule_date'),
            'schedule_time' => implode(':', explode(',', (string)$this->getAreaConfig('schedule_time'))),
            'schedule_from' => (string)$this->getAreaConfig('schedule_from'),
            'schedule_to' => (string)$this->getAreaConfig('schedule_to'),
            'schedule_period' => (int)$this->getAreaConfig('schedule_period'),
        ];
    }

    /**
     * Get feed file config value
     *
     * @param string $fieldShort
     * @param int $storeId
     *
     * @return string
     */
    public function getFeedFileConfig(string $fieldShort, int $storeId = 0): string
    {
        return $this->getConfigValue(Constants::SYTE_CONFIG_PATH_PRODUCT_FEED_FILE . $fieldShort, $storeId);
    }
}
