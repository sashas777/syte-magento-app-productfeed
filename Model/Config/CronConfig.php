<?php

/**
 * Syte_ProductFeed
 */

declare(strict_types=1);

namespace Syte\ProductFeed\Model\Config;

use Magento\Framework\App\Config\Value;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\Config\ValueFactory;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Exception\LocalizedException;
use Syte\Core\Model\Constants;

class CronConfig extends Value
{
    /** @const string */
    private const CRON_STRING_PATH = 'crontab/default/jobs/syte_product_feed_export/schedule/cron_expr';

    /** @const string */
    private const CRON_MODEL_PATH = 'crontab/default/jobs/syte_product_feed_export/run/model';

    /** @const string */
    private const CRON_DATA_PATH_COMMON = 'groups/product_feed/fields/';

    /** @const int */
    private const CRON_MINUTES_DEFAULT = 10;

    /**
     * @var ValueFactory
     */
    private $configValueFactory;

    /**
     * @var string
     */
    private $runModelPath = '';

    /**
     * @param Context $context
     * @param Registry $registry
     * @param ScopeConfigInterface $config
     * @param TypeListInterface $cacheTypeList
     * @param ValueFactory $configValueFactory
     * @param AbstractResource|null $resource
     * @param AbstractDb|null $resoresourceCollectionurce
     * @param string $runModelPath
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        ScopeConfigInterface $config,
        TypeListInterface $cacheTypeList,
        ValueFactory $configValueFactory,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        $runModelPath = '',
        array $data = []
    ) {
        $this->runModelPath = $runModelPath;
        $this->configValueFactory = $configValueFactory;
        parent::__construct($context, $registry, $config, $cacheTypeList, $resource, $resourceCollection, $data);
    }

    /**
     * Get config value by field id
     *
     * @param string $key
     *
     * @return mixed
     */
    private function getDataValueByKey(string $key)
    {
        return $this->getData(self::CRON_DATA_PATH_COMMON . $key . '/value');
    }

    /**
     * Processing object after save data
     *
     * {@inheritdoc}
     *
     * @return $this
     */
    public function afterSave()
    {
        $type = (int)$this->getDataValueByKey('schedule_type');
        switch ($type) {
            case Constants::SYTE_SCHEDULE_ONCE:
                $datetime = strtotime($this->getDataValueByKey('schedule_date')
                    . ' ' . join(':', $this->getDataValueByKey('schedule_time')));
                $cronExprArray = [
                    (int)date('i', $datetime),
                    (int)date('H', $datetime),
                    (int)date('d', $datetime),
                    (int)date('m', $datetime),
                    '*',
                ];
                $cronExprString = join(' ', $cronExprArray);
                break;
            case Constants::SYTE_SCHEDULE_INTERVAL:
                $period = (int)$this->getDataValueByKey('schedule_period');
                $cronExprArray = [
                    self::CRON_MINUTES_DEFAULT,
                    ($period < 24) ? ('*/' . $period) : 0,
                    '*',
                    '*',
                    '*',
                ];
                $cronExprString = join(' ', $cronExprArray);
                break;
            default:
                $cronExprString = '0 0 30 2 *';
        }
        try {
            $this->configValueFactory->create()
                ->load(self::CRON_STRING_PATH, 'path')
                ->setValue($cronExprString)
                ->setPath(self::CRON_STRING_PATH)
                ->save();
            $this->configValueFactory->create()
                ->load(self::CRON_MODEL_PATH, 'path')
                ->setValue($this->runModelPath)
                ->setPath(self::CRON_MODEL_PATH)
                ->save();
        } catch (\Exception $e) {
            throw new LocalizedException(__('We can\'t save the cron expression.'));
        }

        return parent::afterSave();
    }
}
