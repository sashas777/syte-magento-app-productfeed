<?php

/**
 * Syte_ProductFeed
 */

declare(strict_types=1);

namespace Syte\ProductFeed\Controller\Adminhtml\Feed;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Syte\ProductFeed\Api\FeedRepositoryInterface as FeedRepository;
use Syte\ProductFeed\Api\Data\FeedInterface;
use Syte\ProductFeed\Model\Feed as FeedModel;

class InlineEdit extends \Magento\Backend\App\Action
{
    /**
     * @var FeedRepository
     */
    protected $feedRepository;

    /**
     * @var JsonFactory
     */
    protected $jsonFactory;

    /**
     * @param Context $context
     * @param FeedRepository $feedRepository
     * @param JsonFactory $jsonFactory
     */
    public function __construct(
        Context $context,
        FeedRepository $feedRepository,
        JsonFactory $jsonFactory
    ) {
        parent::__construct($context);
        $this->feedRepository = $feedRepository;
        $this->jsonFactory = $jsonFactory;
    }

    /**
     * Authorization level
     *
     * @see _isAllowed()
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Syte_ProductFeed::edit');
    }

    /**
     * Executable
     *
     * @return jsonFactory
     */
    public function execute()
    {
        $resultJson = $this->jsonFactory->create();
        $error = false;
        $messages = [];
        $postItems = $this->getRequest()->getParam('items', []);
        if (!($this->getRequest()->getParam('isAjax') && count($postItems))) {
            return $resultJson->setData(
                [
                'messages' => [__('Please correct the data')],
                'error' => true,
                ]
            );
        }
        foreach (array_keys($postItems) as $id) {
            $entity = $this->feedRepository->getById((int)$id);
            try {
                $entityData = $postItems[$id];
                $extendedEntityData = $entity->getData();
                $this->setEntityData($entity, $extendedEntityData, $entityData);
                $this->feedRepository->save($entity);
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $messages[] = $this->getErrorWithId($entity, $e->getMessage());
                $error = true;
            } catch (\RuntimeException $e) {
                $messages[] = $this->getErrorWithId($entity, $e->getMessage());
                $error = true;
            } catch (\Exception $e) {
                $messages[] = $this->getErrorWithId(
                    $entity,
                    __('Something went wrong while saving the record')
                );
                $error = true;
            }
        }

        return $resultJson->setData(
            [
            'messages' => $messages,
            'error' => $error
            ]
        );
    }

    /**
     * Get extended error message
     *
     * @param FeedInterface $entity
     * @param string $errorText
     *
     * @return string
     */
    protected function getErrorWithId(FeedInterface $entity, string $errorText): string
    {
        return '[Record ID: ' . $entity->getFeedId() . '] ' . $errorText;
    }

    /**
     * Set model data
     *
     * @param FeedModel $entity
     * @param array $extendedEntityData
     * @param array $entityData
     *
     * @return self
     */
    public function setEntityData(FeedModel $entity, array $extendedEntityData, array $entityData)
    {
        $entity->setData(array_merge($entity->getData(), $extendedEntityData, $entityData));

        return $this;
    }
}
