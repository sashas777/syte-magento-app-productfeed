<?php

/**
 * Syte_ProductFeed
 */

declare(strict_types=1);

namespace Syte\ProductFeed\Controller\Adminhtml\Feed;

use Magento\Backend\App\Action\Context;
use Magento\Backend\App\Action;
use Syte\ProductFeed\Api\FeedRepositoryInterface;

class Delete extends Action
{
    /**
     * @var FeedRepositoryInterface
     */
    private $feedRepository;

    /**
     * @param Context $context
     * @param FeedRepositoryInterface $feedRepository
     */
    public function __construct(
        Context $context,
        FeedRepositoryInterface $feedRepository
    ) {
        $this->feedRepository = $feedRepository;
        parent::__construct($context);
    }

    /**
     * Authorization level
     *
     * @see _isAllowed()
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Syte_ProductFeed::edit');
    }

    /**
     * Executable
     *
     * @return resultRedirect
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id = (int)$this->getRequest()->getParam('feed_id')) {
            try {
                $this->feedRepository->deleteById($id);
                $this->messageManager->addSuccess(__('The record has been deleted'));
                $this->_eventManager->dispatch(
                    'adminhtml_syteproductfeed_on_delete',
                    ['feed_id' => $id, 'status' => 'success']
                );
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->_eventManager->dispatch(
                    'adminhtml_syteproductfeed_on_delete',
                    ['feed_id' => $id, 'status' => 'fail']
                );
                $this->messageManager->addError($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['feed_id' => $id]);
            }
        }
        $this->messageManager->addError(__('Cannot find a record to delete'));

        return $resultRedirect->setPath('*/*/');
    }
}
