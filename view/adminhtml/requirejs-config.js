/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    map: {
        '*': {
            editableSelect:         'Syte_ProductFeed/js/jquery-editable-select',
        }
    },

};
