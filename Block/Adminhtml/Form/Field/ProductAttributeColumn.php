<?php

/**
 * Syte_ProductFeed
 */

declare(strict_types=1);

namespace Syte\ProductFeed\Block\Adminhtml\Form\Field;

use Magento\Framework\View\Element\Html\Select;
use Syte\ProductFeed\Model\Config\Source\ProductAttribute;

class ProductAttributeColumn extends Select
{
    /**
     * Set "name" for <select> element
     *
     * @param string $value
     *
     * @return $this
     */
    public function setInputName($value)
    {
        return $this->setName($value);
    }

    /**
     * Set "id" for <select> element
     *
     * @param $value
     *
     * @return $this
     */
    public function setInputId($value)
    {
        return $this->setId($value);
    }

    /**
     * Render block HTML
     *
     * @return string
     */
    public function _toHtml(): string
    {
        if (!$this->getOptions()) {
            $this->setOptions($this->getSourceOptions());
        }
        $script = ($id = $this->getId())
            ? ("<script>require(['jquery', 'editableSelect', 'domReady' ],function($, editableSelect){ $.fn.editableSelect = editableSelect; let obj=$('#" . $id . "');"
                . "if(!obj.val()) obj.find('option[value=\"\"]').attr('selected',true);"
                . 'obj.' . (ProductAttribute::ATTRIBUTES_AUTOSUGGEST ? "editableSelect" : "show") . "();})</script>")
            : '';

        return parent::_toHtml() . $script;
    }

    /**
     * Get select box options
     *
     * @return string
     */
    private function getSourceOptions(): array
    {
        $options = $this->getData('custom_source_options');

        return empty($options) ? [] : $options;
    }
}
