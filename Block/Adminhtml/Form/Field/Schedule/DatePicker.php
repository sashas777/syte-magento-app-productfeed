<?php

/**
 * Syte_ProductFeed
 */

declare(strict_types=1);

namespace Syte\ProductFeed\Block\Adminhtml\Form\Field\Schedule;

use Magento\Framework\Stdlib\DateTime;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;

class DatePicker extends Field
{
    /**
     * @param AbstractElement $element
     */
    public function render(AbstractElement $element)
    {
        $element->setDateFormat(DateTime::DATE_INTERNAL_FORMAT);
        $element->setShowsTime(null);

        return parent::render($element);
    }
}
