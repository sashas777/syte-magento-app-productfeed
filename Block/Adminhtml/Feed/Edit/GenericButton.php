<?php

/**
 * Syte_ProductFeed
 */

declare(strict_types=1);

namespace Syte\ProductFeed\Block\Adminhtml\Feed\Edit;

use Magento\Backend\Block\Widget\Context;
use Syte\ProductFeed\Api\FeedRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\LocalizedException;

class GenericButton
{
    /**
     * @var Context
     */
    protected $context;

    /**
     * @var FeedRepositoryInterface
     */
    protected $feedRepository;

    /**
     * Constructor
     *
     * @param Context $context
     * @param FeedRepositoryInterface $feedRepository
     *
     * @return void
     */
    public function __construct(
        Context $context,
        FeedRepositoryInterface $feedRepository
    ) {
        $this->context = $context;
        $this->feedRepository = $feedRepository;
    }

    /**
     * Get entity by id
     *
     * @return FeedRepositoryInterface|null
     */
    public function getOwnEntityId()
    {
        try {
            return $this->feedRepository->getById(
                (int)$this->context->getRequest()->getParam('feed_id')
            )->getFeedId();
        } catch (NoSuchEntityException $e) {
            throw new LocalizedException(__('Sorry, but we can\'t find the entity you selected.'), $e);
        }

        return null;
    }

    /**
     * Get actions url
     *
     * @param string $route
     * @param array $params
     *
     * @return string
     */
    public function getUrl($route = '', $params = []): string
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
