<?php

/**
 * Syte_ProductFeed
 */

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::MODULE, 'Syte_ProductFeed', __DIR__);
